"""

    Dieses Skript erklärt die Möglicheiten Dateien auszulesen und zu speichern

    (c) David Nagl 2017

"""

def main():
    #LESEN

    pfad = '/home/testfile.txt'                 #Definiert den Pfad der Datei
    """
        Öffnet die Datei mit dem Parameter  'r' --> lesen [DEFAULT]
                                            'w' --> schreiben
                                            '+' --> lesen und schreiben

                                            'x' --> erstellen wenn die datei noch nicht existiert
                                            'a' --> datei erweitern falls diese existiert

                                            'b' --> binärer modus
                                            't' --> text modus [DEFAULT]
    """
    testfile = open(pfad, 'r')
    inhalt = testfile.read()
    print(inhalt)

    #SCHREIBEN
    pfad = '/home/testfile.txt'  #Definiert den Pfad der Datei
    testfile = open(pfad, 'w')
    testfile.write("DIES IST EIN TEST")
    print(testfile.read())


main()