"""

    Dieses Skript zeigt die Grundfunktion der Bibliothek rpi.gpio

    (c) David Nagl 2017

"""

import RPi.GPIO as GPIO
import atexit

def GPIOinit():
    GPIO.setmode(GPIO.BCM)              #Mögliche Werte: GPIO.BCM --> BCM-Layout, GPIO.BOARD --> BOARD-Layout. Wichtig für verwendung der Pins
    GPIOsetup()

def GPIOsetup():
    GPIO.setup(19, GPIO.IN)             #Definiert den Pin 19 (BCM-Layout) als Eingang
    GPIO.setup(21, GPIO.OUT)            #Definiert den Pin 21 (BCM-Layout) als Ausgang

def GPIOset():
    GPIO.output(21, 0)                  #Setzt den Ausgang (Pin 21) auf LOW
    GPIO.output(21, 1)                  #Setzt den Ausgang (Pin 21) auf HIGH

def main():
    GPIOinit()
    GPIOset()

@atexit.register                        #Registiert die Methode als Shutdown-Hook --> Wird beim beenden des Skriptes ausgeführt
def shutdownHook():
    GPIO.cleanup()                      #Gibt alle Pins wieder für weitere Verwendung frei und entfernt die gesetzten Werte

main()