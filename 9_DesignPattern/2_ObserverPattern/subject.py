class Subject:
    observerList = []

    def __init__(self):
        self = self

    def notifyAll(self):
        for element in self.observerList:
            element.notify()

    def changeValue(self):
        self.notifyAll()

    def addObserver(self, observer):
        self.observerList.append(observer)