from threading import Thread
from observer import Observer
from subject import Subject

subject = Subject()

def main():
    observer1 = Observer("OBSERVER 1")
    observer2 = Observer("OBSERVER 2")
    observer3 = Observer("OBSERVER 3")
    observer4 = Observer("OBSERVER 4")

    subject.addObserver(observer1)
    subject.addObserver(observer2)
    subject.addObserver(observer3)
    subject.addObserver(observer4)

    thread = Thread(target=subject.changeValue)
    thread.start()
    thread.join()

def changeValue():
    subject.changeValue()

main()