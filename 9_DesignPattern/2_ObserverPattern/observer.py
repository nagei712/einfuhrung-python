class Observer:
    text = ""

    def __init__(self, text):
        self.text = text

    def notify(self):
        print(self.text)