import multiprocessing
import log, led
import time

def main():
    log.info("Starting Application ...")

    ledQueue = multiprocessing.Queue()
    ledProcess = multiprocessing.Process(target=led.Led().run, args=(ledQueue, ))

    ledProcess.start()

    time.sleep(5)
    ledQueue.put("OBJ 1")

    time.sleep(2)
    ledQueue.put("OBJ 2")

    time.sleep(2)
    ledQueue.put("OBJ 3")

    ledQueue.close()
    ledQueue.join_thread()
    ledProcess.join()

if __name__ == '__main__':
    main()