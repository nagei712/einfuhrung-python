class EffectElem:
    duration = 0
    red = 0
    green = 0
    blue = 0

    def __init__(self, duration, red, green, blue):
        self.duration = duration
        self.red = red
        self.green = green
        self.blue = blue