import datetime
from threading import Thread

def write(msg):
    print(str('{0:%Y-%m-%d %H:%M:%S}'.format(datetime.datetime.now())) + " " + msg)

def setOutput(msg):
    thread = Thread(target=write, args=(msg,))
    thread.start()
    thread.join()

def info(msg):
    setOutput("[*] " + msg)

def error(msg):
    setOutput("[-] " + msg)

def warning(msg):
    setOutput("[!] " + msg)