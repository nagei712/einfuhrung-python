"""

    Dieses Skript erkärt die Verwendung von Methoden zur geordneten Struktur eines Skriptes
    Grundsätzlich sollten Methoden verwendet werden um dem Skript eine Struktur zu verleihen und Codeduplikationen zu vermeiden.

    (c) David Nagl 2017

"""

#Eine Methode wird mit dem Keyword 'def' gekennzeichnet. Nach diesem Keyword wird der Name der Methode definiert.
def main():
    print("Applikation gestartet ...")
    summe(1, 19)                #Hier wird die Methode "summe" aufgerufen und die Werte 1 und 19 werden übergeben.

#Um Methoden einen Wert zu übergeben werden Parameter verwendet welche in die Klammern nach dem Methodennamen definiert werden.
#Diese Parameter müssen bei jedem Aufruf einer Methode übergeben werden.
def summe(a, b):
    print( str( a + b ) )       #str() erzeugt eine Zeichenkette welche von Python augegeben werden kann.


main()          #So wird die Methode mit dem Namen 'main' aufgerufen