"""

    Dieses Skript erkärt die Verwendung von Variablen und Schleifen

    (c) David Nagl 2017

"""

"""
    Variablen

    Variablen werden verwendet um Werte für während der Laufzeit des Skriptes zu speichern.
"""

a = 5                           #Die Variable a wird erzeugt und erhält den Wert 5
b = 10
c = a + b                       #Hier wird die Variable c erzeugt und erhält den Wert der Summe aus a und b

test = "Dies ist ein String."   #Zeichenketten auch Strings genannt können in Variablen gespeichert werden.

bool = True                     #Auch Zustände (True, False) können in einer Variable gespeichert werden.

"""
    Schleifen

    Sollten mehrere Werte nacheinander verwendet werden, ist es möglich Listen zu benutzen und diese mithilfe von Schleifen abzuarbeiten.
"""

liste = ["Das", "ist", "eine", "Liste", "welche", "in", "einer", "Schleife", "verwendet", "wird"]

"""
    Mit dieser For-Schleife werden alle Elemente in der Liste abgearbeitet.
    Dabei wird bei jedem Durchgang eine Variable mit dem Namen 'element' erstellt und diese auf der Konsole ausgegeben.
"""
for element in liste:
    print( element + " " )