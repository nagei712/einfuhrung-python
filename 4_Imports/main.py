"""

    Dieses Skript zeigt die Möglichkeiten der Imports auf

    (c) David Nagl 2017

"""
"""
    Um bereits vorgefertigte Methoden und Funktionalitäten zu verwenden werden sogenannte Bibliotheken verwendet.
    Um diese verwenden zu können müssen die jeweiligen Bibliotheken mit dem Keyword 'import' importiert werden.
"""
import time                                 #Import der Bibliothek 'time'

def main():
    print("Starten der Applikation ...")
    time.sleep(1)                           #Pausieren des Skriptes für 1 Sekunde
    print("5")
    time.sleep(1)
    print("4")
    time.sleep(1)
    print("3")
    time.sleep(1)
    print("2")
    time.sleep(1)
    print("1")
    time.sleep(1)
    print("Ende der Applikation")

main()