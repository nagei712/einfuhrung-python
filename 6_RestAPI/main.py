"""

    COMMAND: pip install bottle

"""
from bottle import run, get, post, delete, request

animals = [
    {'name': 'Ellie', 'type': 'Elephant'},
    {'name': 'Python', 'type': 'Snake'},
    {'name': 'Zed', 'type': 'Zebra'}
]

@get('/animal')
def getAll():
    return {'animals': animals}

@get('/animal/<name>')
def getByName(name):
    selectedAnimal = [animal for animal in animals if animal['name'] == name]
    return {'animal': selectedAnimal[0]}

@post('/animal')
def addOne():
    animal = {'name': request.json.get('name'), 'animal': request.json.get('type')}
    animals.append(animal)
    return getAll()

@delete('/animal/<name>')
def deleteOne(name):
    selectedAnimal = [animal for animal in animals if animal['name'] == name]
    animals.remove(selectedAnimal[0])
    return getAll()

run(reloader=True, port=9000, debug=True)