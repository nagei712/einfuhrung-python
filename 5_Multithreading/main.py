"""

    Dieses Skript zeigt die Grundfunktion des Multithreadings in Python

    (c) David Nagl 2017

"""
import time                                                         #Die Bibliothek "time" beinhaltet verschiedene Methoden welche auf das Thema Zeit bezogen sind
from threading import Thread                                        #Die Bibliothek "threading" beinhaltet Klassen und Methoden um 5_Multithreading zu ermöglichen. Dabei wird in diesem Beispiel nur die Klasse Thread benötigt.

                                                                    #Diese Methode wird als Hauptfunktion beim Starten des Skriptes aufgerufen
def main():
    print("Starten des Skriptes ...")                               #Ausgabe auf der Konsole
    thread1 = Thread(target=asyncTask, args=("Thread 1", 0.05))     #Hier wird ein neues Thread Objekt erzeugt welches im späteren Verlauf asynchron gestartet werden kann.
    thread2 = Thread(target=asyncTask, args=("Thread 2", 0.1))      #Dabei geben der Parameter "target" die Zielmethode und der Parameter "args" die Parameter an welche der Methode übergeben werden.

    thread1.start()                                                 #Hier wird ein neuer Thread gestartet.
    thread2.start()

    thread1.join()                                                  #Um das Skript solange bis alle Threads beendet worden sind vor dem Beenden zu hindern, wird hier auf den 1. Thread gewartet, bis dieser beendet ist.
    thread2.join()

                                                                    #Diese Methode fürht eine Schleife aus und gibt den Namen des Threades und die aktuelle Anzahl der Durchläufe an
                                                                    #Dieser Methode werden 2 Parameter übergeben.
                                                                    #threadName ist der Name des Threads in welchem die Methode ausgeführt wird
                                                                    #delay ist der Wert in Sekunden welcher das Warten zwischen den Durchgängen reguliert
def asyncTask(threadName, delay):
    cnt = 0
    while cnt < 15:
        time.sleep(delay)
        cnt += 1
        print(threadName + ": " + str(cnt))


main()                                                              #Aufruf der "main" Methode
