"""

    Dieses Skript zeigt die M�glichkeiten mit Klassen auf

    (c) David Nagl 2017

"""
"""
    Dies ist die Definition einer Klasse. Eine Klasse wird als Prototyp/Vorlage verwendet um Objekte mit verschiedenen Eigenschaften zu erzeugen.
    Die Klassendefinition verwendet das Keyword "class" welchem der Klassenname folgt.

    Die Methode "__init__" ist der sogenannte Konstruktor und erzeugt ein Objekt dieser Klasse.

    Die Methode "ausgeben" gibt auf der Konsole die Marke und Farbe des Autos aus.
"""
class Auto:
    _farbe = ""
    _marke = ""

    def __init__(self, farbe="", marke=""):
        self._farbe = farbe
        self._marke = marke

    def ausgeben(self):
        print("Das Auto der Marke: " + self._marke + " hat die Farbe " + self._farbe)


def main():
    print("Starten der Applikation ...")

    bmw = Auto("wei�", "BMW")
    audi = Auto("schwarz", "Audi")
    
    """
        Werden die Parameter nicht beim Erzeugen eines Objektes �bergeben, kann auf diese �ber das Objekt zugegriffen werden. 
        Dabei k�nnen diese Werte ausgelesen bzw. gesetzt werden.
    """
    
    vw = Auto()
    vw._farbe = "rot"
    vw._marke = "VW"

    print(bmw.ausgeben())
    print(audi.ausgeben())
    print(vw.ausgeben())
    
main()